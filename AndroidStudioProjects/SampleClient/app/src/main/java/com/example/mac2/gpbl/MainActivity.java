package com.example.mac2.gpbl;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void sendToServer(View view) {
        String ip = ((EditText) findViewById(R.id.ip)).getText().toString();
        String port = ((EditText) findViewById(R.id.port)).getText().toString();
        String data = ((EditText) findViewById(R.id.data)).getText().toString();
        TextView tv = findViewById(R.id.result);
        MyTask task = new MyTask(tv);
        task.execute(ip, port, data);
    }
}
