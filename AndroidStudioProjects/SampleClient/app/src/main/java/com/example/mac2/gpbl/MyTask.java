package com.example.mac2.gpbl;

import android.os.AsyncTask;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;

/**
 * Created by katagai on 2018/02/22.
 */

public class MyTask extends AsyncTask<String, Integer, String> {
    private TextView tv;
    public MyTask(TextView tv) {
        this.tv = tv;
    }

    @Override
    protected String doInBackground(String... strings){ //非同期で処理したい内容記述
        String msg = "error";
        try{
            Socket sock = new Socket(strings[0],Integer.parseInt(strings[1])); // ip＆port指定
            OutputStream os = sock.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os);
            BufferedWriter bw = new BufferedWriter(osw);
            bw.write(strings[2]);   //write data
            bw.flush();
            InputStream in = sock.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            StringBuilder sb = new StringBuilder();
            String b;
            while ((b=br.readLine()) != null){
                sb.append(b);
            }
            msg = sb.toString();
            bw.close();
            br.close();
            sock.close();
        }catch(Exception e){
            e.getStackTrace();
        }
        return msg;
    }

    @Override
    protected void onPostExecute(String msg) { //UIスレッドで実行される. doInBackgroundの返り値が引数
        tv.setText(msg);
    }
}
