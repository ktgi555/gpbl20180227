#include <ESP8266WiFi.h>

// 無線LAN設定
#define SSID    "Fukuda's Wireless Network"
#define WIFIPWD "fkd13q32/"

// サーバ設定
#define PORT     8888            // 起動サーバのポート番号
#define TIMEOUT  20000             // 接続タイムアウト [ms]

// メッセージデータベース
#define MAX_ID_LEN   9             // IDの最長文字数
#define MAX_MSG_LEN  64            // メッセージ最長文字数
#define MAX_ENTRY    100           // データベースに記録できる登録数

// エラー番号基本値
#define ERR_COMMAND  1             // エラーコード（コマンドエラー）
#define ERR_REGISTER 10            // エラーコード（sendコマンド時のエラー）
#define ERR_SEND     20            // エラーコード（receiveコマンド時のエラー）
#define ERR_CHECK    30            // エラーコード（checkコマンド時のエラー）
#define ERR_DELETE   40            // エラーコード（deleteコマンド時のエラー）
#define NO_MESG      String("No Message")  // 登録メッセージがないときのメッセージ
#define ERR_MESG     String("ERROR ")      // エラー時のメッセージ
#define OK_MESG      String("OK ")         // 正常時のメッセージ


// メッセージデータベース構造体定義
typedef struct {
  char to[MAX_ID_LEN];
  char from[MAX_ID_LEN];
  char msg[MAX_MSG_LEN];
} MSGDB;
MSGDB DB[MAX_ENTRY];

// 保存しているメッセージ数
int NumDB = 0;

// 動作確認用内蔵LED
const int onled = 14;

// TCP サーバの設定
WiFiServer server(PORT);
WiFiClient client;

// LEDを点滅させる関数: blink
// pin ....... ピン番号
// onTime .... 点灯時間
// offTime ... 消灯時間
void blink(int pin, int onTime, int offTime) {
  digitalWrite(pin, HIGH);
  delay(onTime);
  digitalWrite(pin, LOW);
  delay(offTime);
}

// メッセージ登録関数 msg_register
String msg_register(String from, String to, String msg) {
  int errno = 0;

  // オプション表示
  Serial.println("[SEND] From: " + from + " / To: " + to + " / Msg: " + msg);

  // DB配列フルエラー
  if (NumDB == MAX_ENTRY) return ERR_MESG + String(ERR_REGISTER + 9);

  // オプションエラーチェック
  if (from.equals("") || from.length() >= MAX_ID_LEN) errno += 1;
  if (to.equals("") || to.length() >= MAX_ID_LEN) errno += 2;
  if (msg.equals("") || msg.length() >= MAX_MSG_LEN) errno += 4;
  if (errno) return ERR_MESG + String(ERR_REGISTER + errno);

  // メッセージの登録
  from.toCharArray(DB[NumDB].from, MAX_ID_LEN);
  to.toCharArray(DB[NumDB].to, MAX_ID_LEN);  
  msg.toCharArray(DB[NumDB].msg, MAX_MSG_LEN);  
  NumDB++;
  return "Message " + msg + " from " + from + " to " + to + " registered";
}


// メッセージ取得関数 msg_retrieve
String msg_retrieve(int num, String usr) {
  for (int i=0; i<NumDB; i++) {
    if (usr.equals(DB[i].to)) {
      if (num == 1) {
        Serial.println("Match to " + String(i));
        return "[" + String(DB[i].from) + "]" + String(DB[i].msg);
      }
      else num--;
    }
  }
  return NO_MESG;
}

// メッセージ削除関数 msg_remove
String msg_remove(int num, String usr) {
  for (int i=0; i<NumDB; i++) {
    if (usr.equals(DB[i].to)) {
      if (num == 1) {
        Serial.println("Match to " + String(i));
        for (int j=i; j<NumDB-1; j++ ) DB[j] = DB[j+1];
        NumDB--;
        return "Message to " + usr + "(" + num + ")" + " deleted";
      }
      else num--;
    }
  }
  return NO_MESG;
}


// メッセージ送信関数  msg_send
String msg_send(String num, String usr) {
  int errno = 0;
  int mode = 0;
  int count = 0;
  String msg = "";
  
  // オプション表示
  Serial.println("[RECV] Num: " + num + " / User: " + usr);
  
  // オプションチェック
  if (num.equalsIgnoreCase("all")) mode = 1;
  else if (num.toInt() == 0) errno += 1;
  if ( usr.equals("") || usr.length() >= MAX_ID_LEN) errno += 2;
  if (errno) return ERR_MESG + String(ERR_CHECK + errno);
  
  // all指定のときの処理
  if (mode) {
    count = msg_check(String("to"),usr).toInt();
    msg += msg_retrieve(1, usr);
    for (int i=1; i<count; i++){
      String stmp = msg_retrieve(i+1, usr);
      if (stmp.equals(NO_MESG)) break;
      else msg += String("\n") + stmp;
    }
  }
  else {
    msg = msg_retrieve(num.toInt(),usr);
  }
  return msg;
}

// メッセージ削除関数  msg_delete
String msg_delete(String num, String usr) {
  int errno = 0;
  int mode = 0;
  int count = 0;
  String msg = "";
  
  // オプション表示
  Serial.println("[DLTE] Num: " + num + " / User: " + usr);
  
  // オプションチェック
  if (num.equalsIgnoreCase("all")) mode = 1;
  else if (num.toInt() == 0) errno += 1;
  if ( usr.equals("") || usr.length() >= MAX_ID_LEN) errno += 2;
  if (errno) return ERR_MESG + String(ERR_CHECK + errno);

  // all指定のときの処理
  if (mode) {
    count = msg_check(String("to"),usr).toInt();
    for (int i=0; i<count; i++){
      String stmp = msg_remove(1, usr);
      if (stmp.equals(NO_MESG)) break;
    }
    return String(count) + " messages to " + usr + " deleted";
  }
  else {
    return msg_remove(num.toInt(),usr);
  }
}

// メッセージ数確認関数 msg_check
String msg_check(String op, String usr) {
  int errno = 0;
  int mode = 0;
  int count = 0;
  
  // オプション表示： toかfromか:ユーザ名
  Serial.println("[CHCK] Opn: " + op + " / User: " + usr);
  
  // オプションチェック
  if (op.equalsIgnoreCase("from")) mode = 1;
  else if (op.equalsIgnoreCase("to")) mode = 2;
  else errno += 1;
  if ( usr.equals("") || usr.length() >= MAX_ID_LEN) errno += 2;
  if (errno) return ERR_MESG + String(ERR_CHECK + errno);

  // メッセージ数の確認
  for (int i=0; i<NumDB; i++ )
    if (mode == 1 && usr.equals(String(DB[i].from))) count++;
    else if (mode == 2 && usr.equals(String(DB[i].to))) count++;

  return String(count);
}


// DB状況表示
void show_status() {
  Serial.println("NumDB = " + String(NumDB));
  Serial.println("ID|  From  |   To   | Message");
  Serial.println("--+--------+--------+---------------------------------------------------------------");
  for (int i=0; i<NumDB; i++) {
    char s[90];
    sprintf(s, "%2d|%8s|%8s|%s", i, DB[i].from, DB[i].to, DB[i].msg);
    Serial.println(String(s));
  }
}

void setup() {
  // 内蔵LEDをDiagに使う，初期値：消灯
  pinMode(onled,OUTPUT);
  digitalWrite(onled, LOW);
  
  // ログ用にシリアルを開く
  Serial.begin(115200);

  //無線LAN AP接続
  Serial.print(String("\nConnecting to ") + SSID);
  WiFi.begin(SSID, WIFIPWD);

  while (WiFi.status() != WL_CONNECTED) { // 接続するまで継続
    blink(onled, 250, 250);
    Serial.print(".");
  }
  // 接続完了メッセージ
  digitalWrite(onled, HIGH);
  Serial.print(" done.\nMy IP address: ");
  Serial.println(WiFi.localIP());
  
  // サーバ起動
  digitalWrite(onled, LOW); //消灯
  delay(250);
  server.begin();
  Serial.println("TCP server started. Port: " + String(PORT));
  digitalWrite(onled, HIGH); 
}

void loop() {
  client = server.available(); // TCP接続が来たらclientインスタンスが生成される
  unsigned long timeout = 0;   // タイムアウト用変数 timeout, init
  boolean init = true;
  
  while(client) {
    // 接続時にタイムアウト時間を設定
    if (init) {
      timeout = millis() + TIMEOUT;
      init = false;
    }

    // メッセージが送信されたときの処理
    while (client.available()) {
      String op1, op2, op3, msg;
      // 受信コマンドの取得
      
      String cmd = client.readStringUntil(':');
      if (cmd.equalsIgnoreCase("send")) {
        op1 = client.readStringUntil(':');
        op2 = client.readStringUntil(':');
        op3 = client.readString();
        msg = msg_register(op1, op2, op3);
      }
      else if (cmd.equalsIgnoreCase("receive")) {
        op1 = client.readStringUntil(':');
        op2 = client.readString();
        msg = msg_send(op1, op2);
      }
      else if (cmd.equalsIgnoreCase("check")) {
        op1 = client.readStringUntil(':');
        op2 = client.readString();
        msg = msg_check(op1, op2);
      }
      else if (cmd.equalsIgnoreCase("delete")) {
        op1 = client.readStringUntil(':');
        op2 = client.readString();
        msg = msg_delete(op1, op2);
      }
      else
        msg = ERR_MESG + String(ERR_COMMAND);

      Serial.println(msg);
      client.print(msg);
      delay(1);

      // ログ表示
      show_status();

      // メッセージを受信するたびに切断処理
      client.stop();
      return;
    }

    // タイムアウト時の切断処理
    if (timeout < millis()) {
      Serial.print("Client Timeout. From: ");
      Serial.println(client.remoteIP());
      client.stop();
      delay(1);
      return;
    }
  }
}
